import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Inject } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: `app.component.html`,
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  nameModel: string;
  typeModel: string;
  foods: object[];
  filteredFoods: object[];

  constructor(@Inject(HttpClient) public http: HttpClient) {
  }

  ngOnInit() {
    this.http.get('http://localhost:3001/food').subscribe((data: object[]) => {
      this.foods = data;
    });
  }
  filterFood() {
    this.filteredFoods = this.foods.filter(el => {
      // @ts-ignore
      return el.name === this.nameModel && el.type === this.typeModel;
    });
  }
}
