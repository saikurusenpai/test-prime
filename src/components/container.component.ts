import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-container',
  templateUrl: 'container.component.html',
  styleUrls: ['./container.component.css']
})
export class AppContainerComponent {
  @Input() foodsList: any;
}
